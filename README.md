# Understanding the influence of depth on the global deep-sea plasmidome


This repository includes the scripts essential for conducting the analysis outlined in the paper "Understanding the influence of depth on the global deep-sea plasmidome" (Fig. 1). The Python scripts are required for generating the feature tables, while the R scripts facilitate the creation of the plots presented in the paper.


![Fig. 1: Pipeline employed to characterize the plasmidomes of whole metagenomes from the deep sea.](pipeline.png)